package com.university.task7

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface RetroApi {
    @GET ("{path}")
    fun getRequest(@Path("path") path: String ): Call<String>
}