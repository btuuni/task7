package com.university.task7

import com.google.gson.annotations.SerializedName

data class Info(
    @SerializedName("descriptionEN")
    val description: String, @SerializedName("titleEN") val title: String, val cover: String
)