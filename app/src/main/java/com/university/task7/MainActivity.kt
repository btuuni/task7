package com.university.task7

import android.os.Bundle
import android.util.Log.d
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.university.task7.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: RecycleAdapter
    private var information = ArrayList<Info>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView(this, R.layout.activity_main) as ActivityMainBinding
        init()

    }


    private fun init() {
        setInfo()
    }

    private fun setInfo() {
        DataLoader.getRequest(
            "5edb4d643200002a005d26f0", object : CustomCallback {
                override fun onSuccess(response: String) {
                    val data = Gson().fromJson(response, Array<Info>::class.java).toList()

                    information.addAll(data)

                    recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
                    adapter = RecycleAdapter(information)
                    recyclerView.adapter = adapter
                }

                override fun onFailed(errorMessage: String) {
                    d("error", errorMessage)
                }
            }
        )
    }
}
